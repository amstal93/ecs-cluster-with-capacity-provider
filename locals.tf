locals {
  # This is the convention we use to know what belongs to each other
  auto_scaling_name      = var.auto_scaling_name != "" ? var.auto_scaling_name : "${var.name}-cluster-asg-instance"
  capacity_provider_name = var.capacity_provider_name != "" ? var.capacity_provider_name : "${local.cluster_name}-prov1"
  cluster_name           = var.cluster_name != "" ? var.cluster_name : "${var.name}-cluster"
  launch_template_name   = var.launch_template_name != "" ? var.launch_template_name : "${var.name}-asg-template"
  security_group_name    = var.security_group_name != "" ? var.security_group_name : "${var.name}-sg"
}

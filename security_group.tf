resource "aws_security_group" "this" {
  name        = local.security_group_name
  description = "${var.name} security group"
  vpc_id      = data.aws_vpc.selected.id

  egress {
    cidr_blocks = ["0.0.0.0/0"] #tfsec:ignore:AWS009
    description = "internet"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = var.tags
}

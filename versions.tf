terraform {
  required_providers {
    aws = {
      version = "~> 3.38"
      source  = "hashicorp/aws"
    }
  }
  required_version = ">= 1.0"
}

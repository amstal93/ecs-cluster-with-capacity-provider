module "ecs" {
  source = "terraform-aws-modules/ecs/aws"

  name               = local.cluster_name
  container_insights = true

  capacity_providers = ["FARGATE", "FARGATE_SPOT", aws_ecs_capacity_provider.prov1.name]

  default_capacity_provider_strategy = [{
    capacity_provider = aws_ecs_capacity_provider.prov1.name
    weight            = "1"
  }]

  tags = var.tags
}

resource "aws_ecs_capacity_provider" "prov1" {
  name = local.capacity_provider_name

  auto_scaling_group_provider {
    auto_scaling_group_arn         = module.asg.autoscaling_group_arn
    managed_termination_protection = "ENABLED"

    managed_scaling {
      minimum_scaling_step_size = 1
      status                    = "ENABLED"
    }
  }
  tags = var.tags

}

module "ec2_profile" {
  source = "terraform-aws-modules/ecs/aws//modules/ecs-instance-profile"
  name   = var.name
  tags   = var.tags
}

data "template_file" "user_data" {
  template = file("${path.module}/templates/user-data.sh")

  vars = {
    cluster_name = local.cluster_name
  }
}

module "asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 4.0"

  name = local.auto_scaling_name

  # Launch template
  lt_name                = local.launch_template_name
  description            = "Launch template for auto scaling group"
  update_default_version = true
  lt_version             = "$Latest"

  use_lt    = true
  create_lt = true

  image_id          = data.aws_ami.amazon_linux_ecs.image_id
  instance_type     = var.instance_type
  ebs_optimized     = true
  enable_monitoring = true
  user_data_base64  = base64encode(data.template_file.user_data.rendered)

  # Auto scaling group
  vpc_zone_identifier       = data.aws_subnet_ids.private.ids
  health_check_type         = "EC2"
  min_size                  = var.min_size
  max_size                  = var.max_size
  desired_capacity          = var.min_size
  wait_for_capacity_timeout = 0
  security_groups           = [aws_security_group.this.id]
  iam_instance_profile_arn  = module.ec2_profile.iam_instance_profile_arn
  protect_from_scale_in     = true

  tags_as_map = var.tags
}


